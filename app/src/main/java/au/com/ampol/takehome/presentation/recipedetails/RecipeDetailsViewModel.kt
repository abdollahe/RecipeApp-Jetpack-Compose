package au.com.ampol.takehome.presentation.recipedetails

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import au.com.ampol.takehome.R
import au.com.ampol.takehome.domain.exceptions.RecipeDetailsSearchException
import au.com.ampol.takehome.domain.usecases.FetchRecipeDetailsUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

class RecipeDetailsViewModel @Inject constructor(
    private val useCase: FetchRecipeDetailsUseCase,
    private val resource : Resources
) : ViewModel() {

    private val _navigationSharedFlow = MutableSharedFlow<RecipeDetailsNavigationAction>()
    val navigationFlow: Flow<RecipeDetailsNavigationAction> = _navigationSharedFlow.asSharedFlow()

    private val _viewStateFlow = MutableStateFlow<ViewState>(ViewState.Loading)
    val viewState = _viewStateFlow.asStateFlow()

    fun prepareViewModel(selectedRecipeId: String) {
        getRecipeDetails(selectedRecipeId)
    }

    fun goBackToRecipeList() {
        viewModelScope.launch {
            _navigationSharedFlow.emit(RecipeDetailsNavigationAction.GoBackAction)
        }
    }

    fun closeApplication() {
        viewModelScope.launch {
            _navigationSharedFlow.emit(RecipeDetailsNavigationAction.CloseApplication)
        }
    }

    private fun getRecipeDetails(recipeId: String) {
        viewModelScope.launch {
            try {
                val recipe = useCase.getRecipeDetails(Dispatchers.IO, recipeId)
                _viewStateFlow.value = ViewState.Ready(recipe = recipe)
            } catch (e: Exception) {
                handleError(e)
            }
        }
    }

    private fun handleError(e: Exception) {
        when(e) {
            is RecipeDetailsSearchException -> {
                _viewStateFlow.value = ViewState.Error(errorMessage = e.message ?: "")
            }
            else -> {
                _viewStateFlow.value =
                    ViewState.Error(
                        errorMessage = resource.getString(R.string.recipe_details_general_error_message) )
            }
        }

    }

}