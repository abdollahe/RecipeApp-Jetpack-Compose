package au.com.ampol.takehome.presentation.recipelist

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.FabPosition
import androidx.compose.material.FloatingActionButton
import androidx.compose.material.Icon
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import au.com.ampol.takehome.R
import au.com.ampol.takehome.presentation.AppToolbar
import au.com.ampol.takehome.presentation.navigation.Screens
import au.com.ampol.takehome.presentation.theme.BodyThreeMedium
import au.com.ampol.takehome.presentation.theme.RecipeAppTheme
import au.com.ampol.takehome.presentation.theme.Subtitle
import au.com.ampol.takehome.presentation.theme.TitleFive
import au.com.ampol.takehome.presentation.theme.TitleSix
import au.com.ampol.takehome.utils.closeActivity
import au.com.ampol.takehome.utils.getDifficultyImageResource
import coil.compose.AsyncImagePainter
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest
import coil.size.Scale
import coil.size.Size

@Composable
fun RecipeListScreen(
    viewModel : RecipeListViewModel,
    navController: NavHostController,
    onRecipeClicked : (String) -> Unit
) {

    val viewState by viewModel.viewState.collectAsState()
    val context = LocalContext.current

    RecipeAppTheme {
        Scaffold(
            topBar = {
                val painter = painterResource(id = R.drawable.ic_baseline_close_24)
                AppToolbar(
                    title = "Recipe App",
                    actionClicked = viewModel::closeApplication,
                    actionIconPainter = painter
                )
                     },
            floatingActionButtonPosition = FabPosition.End,
            floatingActionButton = { FloatingActionButton(onClick = { viewModel.refreshScreen() })
            {
                Icon(
                    painter = painterResource(id = R.drawable.ic_baseline_refresh_24),
                    contentDescription = null
                )
            }
                                   },
            content = { paddingValue ->
                when(viewState) {
                    ViewState.Loading -> {
                        RecipeListLoading(modifier = Modifier.padding(paddingValue))
                    }
                    is ViewState.Error -> {
                        val state = (viewState as ViewState.Error)
                        ErrorLoadingContent(
                            modifier = Modifier.padding(paddingValue),
                            state.errorMessage)
                    }
                    is ViewState.Ready -> {
                        val state = (viewState as ViewState.Ready)
                        RecipeListContent(
                            modifier = Modifier.padding(paddingValue) ,
                            recipeList = state.recipeList ,
                            viewModel = viewModel
                        )
                    }
                }
            }
        )
    }

    LaunchedEffect(key1 = "navigation") {
        viewModel.navigationFlow.collect { navigation ->
            when (navigation) {
                RecipeListNavigationAction.CloseApplication -> {
                    context.closeActivity()
                }
                 is RecipeListNavigationAction.ShowRecipeDetails -> {
                    onRecipeClicked(navigation.selectedRecipeId)
                    navController.navigate(Screens.RecipeDetailsScreen.route)
                }
            }
        }
    }
}

@Composable
fun ErrorLoadingContent(modifier : Modifier = Modifier , message : String) {
    Box(modifier = modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        Column(horizontalAlignment = Alignment.CenterHorizontally) {
            Image(
                painter = painterResource(id = R.drawable.error_icon_32),
                contentDescription = null,
                modifier = Modifier
                    .padding(start = 4.dp)
                    .size(200.dp)
                    .clip(
                        RoundedCornerShape(20.dp)
                    )
            )
            Text(
                modifier = Modifier.padding(vertical = 4.dp , horizontal = 8.dp),
                text = message,
                style = TitleFive,
                textAlign = TextAlign.Center
            )
        }
    }
}

@Composable
fun RecipeListLoading(modifier: Modifier = Modifier) {
    Box(modifier = modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        CircularProgressIndicator()
    }
}

@Composable
fun RecipeListContent(modifier: Modifier = Modifier , recipeList : List<RecipeItemListedPresentation>, viewModel : RecipeListViewModel) {
    LazyColumn(modifier = modifier.padding(8.dp)) {
        items(items = recipeList) { recipeItem ->
            RecipeListItemCard(recipeItem = recipeItem , viewModel::goToNextScreen)
        }
    }
}

@Composable
fun RecipeListItemCard(recipeItem : RecipeItemListedPresentation, onItemClicked : (String) -> Unit) {

    Card(modifier = Modifier
        .fillMaxWidth()
        .padding(8.dp)
        .clickable {
            onItemClicked(recipeItem.recipeId)
        } ,
    elevation = 10.dp) {

        Row(modifier = Modifier.fillMaxWidth() , verticalAlignment = Alignment.CenterVertically) {
            val painter = rememberAsyncImagePainter(
                model = ImageRequest.Builder(LocalContext.current)
                    .data(recipeItem.thumb)
                    .size(Size(100 , 100) )
                    .scale(Scale.FILL)
                    .build()
            )
            if (painter.state is AsyncImagePainter.State.Success) {
                Image(
                    painter = painter,
                    contentDescription = null,
                    modifier = Modifier
                        .padding(start = 4.dp)
                        .size(80.dp)
                        .clip(
                            RoundedCornerShape(20.dp)
                        )
                )
            }

            Column {
                Text(
                    modifier = Modifier.padding(top = 8.dp , start = 4.dp),
                    text = recipeItem.recipeName,
                    style = TitleSix.copy(fontWeight = FontWeight.Bold)
                )
                Text(
                    modifier = Modifier.padding(start = 4.dp),
                    text = recipeItem.recipeHeadline ,
                    style = Subtitle.copy(fontWeight = FontWeight.Normal)
                )
                Row(modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 4.dp, top = 4.dp, bottom = 8.dp),
                    horizontalArrangement = Arrangement.SpaceBetween)
                {
                    Text(
                        text = recipeItem.calories,
                        style = BodyThreeMedium)
                    Row(modifier = Modifier
                        .padding(end = 8.dp),
                        verticalAlignment = Alignment.CenterVertically)
                    {
                        Text(
                            modifier = Modifier.padding(end = 4.dp),
                            text = "Difficulty:" , style = BodyThreeMedium
                        )
                        Icon(
                            painter = painterResource(id = getDifficultyImageResource(recipeItem.difficultyLevel)),
                            contentDescription = null,
                            tint = Color.Unspecified,
                            modifier = Modifier.size(15.dp)
                        )
                    }
                }
            }
        }
    }
}


