package au.com.ampol.takehome.presentation.recipedetails

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.clipToBounds
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import au.com.ampol.takehome.R
import au.com.ampol.takehome.presentation.AppToolbar
import au.com.ampol.takehome.presentation.theme.BodyThreeMedium
import au.com.ampol.takehome.presentation.theme.RecipeAppTheme
import au.com.ampol.takehome.presentation.theme.TitleFive
import au.com.ampol.takehome.presentation.theme.TitleFour
import au.com.ampol.takehome.presentation.theme.TitleSeven
import au.com.ampol.takehome.presentation.theme.TitleSix
import au.com.ampol.takehome.utils.closeActivity
import au.com.ampol.takehome.utils.getDifficultyImageResource
import coil.compose.AsyncImagePainter
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest
import coil.size.Scale
import coil.size.Size

@Composable
fun RecipeDetailsScreen(
    viewModel: RecipeDetailsViewModel,
    navController: NavHostController
) {

    val viewState by viewModel.viewState.collectAsState()
    val context = LocalContext.current

    RecipeAppTheme {

        Scaffold(
            topBar = {
                val actionPainter = painterResource(id = R.drawable.ic_baseline_close_24)
                val navigationPainter = painterResource(id = R.drawable.ic_baseline_arrow_back_24)
                AppToolbar(
                    title = "Recipe App",
                    actionClicked = viewModel::closeApplication,
                    actionIconPainter = actionPainter,
                    navigationIconPainter = navigationPainter,
                    navigationIconClicked = viewModel::goBackToRecipeList
                )
            },
            content = { paddingValue ->
                when (viewState) {
                    ViewState.Loading -> {
                        RecipeLoading(modifier = Modifier.padding(paddingValue))
                    }
                    is ViewState.Error -> {
                        ErrorLoadingContent(
                            modifier = Modifier.padding(paddingValue),
                            (viewState as ViewState.Error).errorMessage
                        )
                    }
                    is ViewState.Ready -> {
                        val state = (viewState as ViewState.Ready)
                        RecipeItemCard(
                            modifier = Modifier.padding(paddingValue),
                            recipeItem = state.recipe
                        )
                    }
                }
            }
        )

        LaunchedEffect(key1 = "navigation") {
            viewModel.navigationFlow.collect { navigation ->
                when (navigation) {
                    RecipeDetailsNavigationAction.CloseApplication -> {
                        context.closeActivity()
                    }
                    is RecipeDetailsNavigationAction.GoBackAction -> {
                        navController.navigateUp()
                    }
                }
            }
        }

    }
}

@Composable
fun ErrorLoadingContent(modifier: Modifier = Modifier, message: String) {
    Box(modifier = modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        Column(horizontalAlignment = Alignment.CenterHorizontally) {
            Image(
                painter = painterResource(id = R.drawable.error_icon_32),
                contentDescription = null,
                modifier = Modifier
                    .padding(start = 4.dp)
                    .size(200.dp)
            )
            Text(
                modifier = Modifier.padding(vertical = 4.dp, horizontal = 8.dp),
                text = message,
                style = TitleFive,
                textAlign = TextAlign.Center
            )
        }
    }
}

@Composable
fun RecipeLoading(modifier: Modifier = Modifier) {
    Box(modifier = modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        CircularProgressIndicator()
    }
}

@Composable
fun RecipeItemCard(modifier: Modifier = Modifier, recipeItem: RecipeItemPresentation) {
    Box(modifier = modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        Column(modifier = Modifier.verticalScroll(rememberScrollState())) {
            Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.Center) {
                val painter = rememberAsyncImagePainter(
                    model = ImageRequest.Builder(LocalContext.current)
                        .data(recipeItem.image)
                        .size(Size.ORIGINAL)
                        .scale(Scale.FILL)
                        .build()
                )
                if (painter.state is AsyncImagePainter.State.Success) {
                    Image(
                        painter = painter,
                        contentDescription = null,
                        modifier = Modifier
                            .padding(start = 4.dp)
                            .size(300.dp)
                            .clip(
                                RoundedCornerShape(20.dp)
                            )
                            .clipToBounds()
                    )
                }
            }
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 8.dp),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(
                    modifier = Modifier.padding(horizontal = 4.dp),
                    text = recipeItem.recipeName,
                    style = TitleFour.copy(fontWeight = FontWeight.Bold)
                )
                Text(
                    modifier = Modifier.padding(horizontal = 4.dp),
                    text = recipeItem.recipeHeadline,
                    style = TitleSix.copy(fontWeight = FontWeight.Normal)
                )
                Text(
                    modifier = Modifier.padding(horizontal = 4.dp),
                    text = recipeItem.description,
                    style = TitleSeven.copy(fontWeight = FontWeight.Normal),
                    textAlign = TextAlign.Justify
                )
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(start = 4.dp, top = 4.dp, bottom = 8.dp),
                    horizontalArrangement = Arrangement.SpaceBetween
                )
                {
                    Text(
                        text = recipeItem.calories,
                        style = BodyThreeMedium
                    )
                    Row(
                        modifier = Modifier.padding(end = 8.dp),
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Text(
                            modifier = Modifier.padding(end = 4.dp),
                            text = "Difficulty:",
                            style = BodyThreeMedium
                        )
                        Icon(
                            painter = painterResource(id = getDifficultyImageResource(recipeItem.difficultyLevel)),
                            contentDescription = null,
                            tint = Color.Unspecified,
                            modifier = Modifier.size(15.dp)
                        )
                    }
                }
            }
        }
    }
}





