package au.com.ampol.takehome

import au.com.ampol.takehome.defaults.AppConfig
import au.com.ampol.takehome.defaults.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

open class TakeHomeApplication: DaggerApplication() {
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent
            .builder()
            .applicationContext(this)
            .appConfig(AppConfig())
            .build()
    }
}