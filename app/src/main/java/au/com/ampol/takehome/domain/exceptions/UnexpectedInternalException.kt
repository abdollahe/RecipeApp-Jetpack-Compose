package au.com.ampol.takehome.domain.exceptions

class UnexpectedInternalException(message : String? = null) :
    DomainException(DomainExceptionCodes.UNEXPECTED_INTERNAL_ERROR , message)