package au.com.ampol.takehome.presentation.navigation

sealed class Screens(val route : String) {
    object RecipeListScreen : Screens("recipe_list_screen")
    object RecipeDetailsScreen : Screens("recipe_details_screen")
}