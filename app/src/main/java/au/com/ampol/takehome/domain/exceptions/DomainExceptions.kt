package au.com.ampol.takehome.domain.exceptions

abstract class DomainException(
    private val errorCode : Int,
    private val errorMessage: String?) : RuntimeException(errorMessage)