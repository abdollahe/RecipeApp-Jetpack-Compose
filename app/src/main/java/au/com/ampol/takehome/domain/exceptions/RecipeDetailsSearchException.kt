package au.com.ampol.takehome.domain.exceptions

class RecipeDetailsSearchException(message : String? = null) :
    DomainException(DomainExceptionCodes.RECIPE_DETAILS_FETCH_ERROR , message)