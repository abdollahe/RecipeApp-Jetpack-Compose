package au.com.ampol.takehome.presentation.theme

import androidx.compose.material.Typography
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp

// Set of Material typography styles to start with

val TitleOne = TextStyle(
    fontFamily = FontFamily.SansSerif,
    fontWeight = FontWeight.Medium,
    fontSize = 42.sp
)

val TitleTwo = TextStyle(
    fontFamily = FontFamily.SansSerif,
    fontWeight = FontWeight.Light,
    fontSize = 24.sp
)

val TitleTwoRegular = TitleTwo.copy(
    fontWeight = FontWeight.Normal
)

val TitleTwoMedium = TitleTwo.copy(
    fontWeight = FontWeight.Medium
)

val TitleThree = TextStyle(
    fontFamily = FontFamily.SansSerif,
    fontWeight = FontWeight.Medium,
    fontSize = 22.sp
)

val TitleFour = TextStyle(
    fontFamily = FontFamily.SansSerif,
    fontWeight = FontWeight.Medium,
    fontSize = 20.sp
)

val TitleFourLight = TitleFour.copy(
    fontWeight = FontWeight.Light
)

val TitleFourRegular = TitleFour.copy(
    fontWeight = FontWeight.Normal
)

val TitleFive = TextStyle(
    fontFamily = FontFamily.SansSerif,
    fontWeight = FontWeight.Medium,
    fontSize = 18.sp
)

val TitleSix = TextStyle(
    fontFamily = FontFamily.SansSerif,
    fontWeight = FontWeight.Medium,
    fontSize = 16.sp
)

val TitleSeven = TextStyle(
    fontFamily = FontFamily.SansSerif,
    fontWeight = FontWeight.Medium,
    fontSize = 14.sp
)

val Subtitle = TextStyle(
    fontFamily = FontFamily.SansSerif,
    fontWeight = FontWeight.Bold,
    fontSize = 13.sp,
    color = darkBlueGrey70,
    fontFeatureSettings = "c2sc, smcp"
)

val BodyOne = TextStyle(
    fontFamily = FontFamily.SansSerif,
    fontWeight = FontWeight.Normal,
    fontSize = 18.sp
)

val BodyTwo = TextStyle(
    fontFamily = FontFamily.SansSerif,
    fontWeight = FontWeight.Normal,
    fontSize = 16.sp
)

val BodyTwoMedium = BodyTwo.copy(
    fontWeight = FontWeight.Medium
)

val BodyThree = TextStyle(
    fontFamily = FontFamily.SansSerif,
    fontWeight = FontWeight.Normal,
    fontSize = 14.sp
)

val BodyThreeMedium = BodyThree.copy(
    fontWeight = FontWeight.Medium
)

val BodyThreeBold = BodyThree.copy(
    fontWeight = FontWeight.Bold
)

val CaptionOne = TextStyle(
    fontFamily = FontFamily.SansSerif,
    fontWeight = FontWeight.Normal,
    fontSize = 13.sp
)

val CaptionOneMedium = CaptionOne.copy(
    fontWeight = FontWeight.Medium
)

val CaptionOneRegular = CaptionOne.copy(
    fontWeight = FontWeight.Normal,
    fontSize = 13.sp
)

val CaptionTwo = TextStyle(
    fontFamily = FontFamily.SansSerif,
    fontWeight = FontWeight.Normal,
    fontSize = 12.sp
)

val CaptionTwoMedium = CaptionTwo.copy(
    fontWeight = FontWeight.Medium
)

val CaptionThree = TextStyle(
    fontFamily = FontFamily.SansSerif,
    fontWeight = FontWeight.Normal,
    fontSize = 10.sp
)

val ButtonTextStyle = TextStyle(
    fontFamily = FontFamily.SansSerif,
    fontWeight = FontWeight.Normal,
    fontSize = 18.sp,
    color = white
)

val OutlinedButtonStyle =
    TextStyle(
        fontFamily = FontFamily.SansSerif,
        fontWeight = FontWeight.Normal,
        fontSize = 13.sp,
        color = charcoalGrey
    )

val typography = Typography(
    h1 = TitleOne,
    h2 = TitleTwo,
    h3 = TitleThree,
    h4 = TitleFour,
    h5 = TitleFive,
    subtitle1 = BodyTwo,
    body1 = BodyOne,
    body2 = BodyTwo,
    button = ButtonTextStyle,
    caption = CaptionOne
)
