package au.com.ampol.takehome.presentation.navigation

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import au.com.ampol.takehome.defaults.di.viewmodel.ViewModelFactory
import au.com.ampol.takehome.presentation.recipedetails.RecipeDetailsScreen
import au.com.ampol.takehome.presentation.recipedetails.RecipeDetailsViewModel
import au.com.ampol.takehome.presentation.recipelist.RecipeListScreen
import au.com.ampol.takehome.presentation.recipelist.RecipeListViewModel
import au.com.ampol.takehome.presentation.theme.popOutEntryTransition
import au.com.ampol.takehome.presentation.theme.slideDownExitTransition
import au.com.ampol.takehome.presentation.theme.slideUpEntryTransition
import com.google.accompanist.navigation.animation.AnimatedNavHost
import com.google.accompanist.navigation.animation.composable

@OptIn(ExperimentalAnimationApi::class)
@Composable
fun Navigation(
    navController: NavHostController,
    factory: ViewModelFactory,
    startDestination: String = Screens.RecipeListScreen.route
) {

    // use rememberSavable to preserve the state even after configuration change
    var selectedRecipeId by rememberSaveable  { mutableStateOf("") }

    AnimatedNavHost(
        navController = navController,
        startDestination = startDestination
    ) {

        composable( route = Screens.RecipeListScreen.route ) {
            val viewModel = viewModel(
                modelClass = RecipeListViewModel::class.java ,
                factory = factory
            )

            viewModel.prepareViewModel()

            RecipeListScreen(
                viewModel = viewModel ,
                navController = navController
            ) { recipeId ->
                selectedRecipeId = recipeId
            }

        }

        composable(
            route = Screens.RecipeDetailsScreen.route ,
            enterTransition = slideUpEntryTransition ,
            exitTransition = slideDownExitTransition ,
            popEnterTransition = popOutEntryTransition
        ) {

            val viewModel = viewModel(
                modelClass = RecipeDetailsViewModel::class.java,
                factory = factory
            )

            viewModel.prepareViewModel(selectedRecipeId = selectedRecipeId)

            RecipeDetailsScreen(
                viewModel = viewModel,
                navController = navController
            )

        }
    }

}