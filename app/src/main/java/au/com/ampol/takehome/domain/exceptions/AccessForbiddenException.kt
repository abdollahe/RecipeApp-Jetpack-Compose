package au.com.ampol.takehome.domain.exceptions

class AccessForbiddenException(message : String? = null) :
    DomainException(DomainExceptionCodes.HTTP_ACCESS_FORBIDDEN_ERROR , message)