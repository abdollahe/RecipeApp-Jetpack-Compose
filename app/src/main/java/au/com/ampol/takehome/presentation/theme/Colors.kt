package au.com.ampol.takehome.presentation.theme

import androidx.compose.ui.graphics.Color


val cerulean = Color(0xff0077c8)
val oceanBlue = Color(0xff006ab3)
val tomato = Color(0xffda291c)
val darkBlueGrey = Color(0xff1f3a4a)
val white = Color(0xffffffff)
val charcoalGrey = Color(0xff363d43)
val darkBlueGrey70 = Color(0xb31f3a4a)


val primary = cerulean
val primaryDark = oceanBlue
val accentError = tomato
val textPrimary = darkBlueGrey
