package au.com.ampol.takehome.presentation.recipelist

sealed class RecipeListNavigationAction {
    data class ShowRecipeDetails(val selectedRecipeId : String) : RecipeListNavigationAction()
    object CloseApplication : RecipeListNavigationAction()
}