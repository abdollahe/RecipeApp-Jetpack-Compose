package au.com.ampol.takehome.presentation

import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.unit.dp
import au.com.ampol.takehome.presentation.theme.cerulean


@Composable
fun AppToolbar(
    title: String,
    actionClicked: (() -> Unit)? = null,
    actionIconPainter: Painter? = null,
    navigationIconClicked: (() -> Unit)? = null,
    navigationIconPainter: Painter? = null,
) {
    TopAppBar(
        title = { Text(text = title) },
        backgroundColor = cerulean,
        navigationIcon = if (navigationIconPainter != null) {
            {
                IconButton(onClick = {navigationIconClicked?.invoke()} )
                {
                    Icon(
                        painter = navigationIconPainter,
                        contentDescription = null,
                        tint = Color.White
                    )
                }
            }

        } else null
        ,
        actions = {
            when {
                actionIconPainter != null -> {
                    IconButton(onClick = {actionClicked?.invoke()})
                    {
                        Icon(
                            painter = actionIconPainter,
                            contentDescription = null,
                            tint = Color.White
                        )
                    }
                }
                else -> {}
            }
        },
        elevation = 10.dp
    )
}


