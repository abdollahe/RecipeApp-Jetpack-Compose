package au.com.ampol.takehome.defaults.di

import android.content.Context
import androidx.fragment.app.Fragment
import au.com.ampol.takehome.TakeHomeApplication
import au.com.ampol.takehome.defaults.AppConfig
import au.com.ampol.takehome.defaults.di.viewmodel.ViewModelFactory
import au.com.ampol.takehome.defaults.di.viewmodel.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AppModule::class,
    AndroidInjectionModule::class,
    AndroidSupportInjectionModule::class,
    AndroidBindingModule::class,
    ShoppingModule::class,
    ViewModelModule::class
])
interface AppComponent : AndroidInjector<TakeHomeApplication> {

    fun provideViewModelFactory(): ViewModelFactory

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun applicationContext(context: Context): Builder

        @BindsInstance
        fun appConfig(appConfig: AppConfig): Builder

        fun build(): AppComponent
    }
}