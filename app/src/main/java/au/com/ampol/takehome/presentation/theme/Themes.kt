package au.com.ampol.takehome.presentation.theme

import androidx.compose.material.MaterialTheme
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable


private val colorPalette = lightColors(
    primary = primary,
    primaryVariant = primaryDark,
    secondary = primary,
    secondaryVariant = primaryDark,
    error = accentError,
    background = white,
    surface = white,
    onSurface = textPrimary,
    onBackground = textPrimary,
    onError = white,
    onPrimary = white,
    onSecondary = white
)



@Composable
fun RecipeAppTheme(
    content : @Composable () -> Unit
) {
    MaterialTheme(
        colors = colorPalette,
        typography = typography,
        shapes = Shapes,
        content = content
    )
}