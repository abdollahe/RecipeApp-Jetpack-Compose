package au.com.ampol.takehome.domain.exceptions

class UnauthorisedAccessException(message : String? = null) :
    DomainException(DomainExceptionCodes.HTTP_UNAUTHORIZED_ACCESS_ERROR , message)