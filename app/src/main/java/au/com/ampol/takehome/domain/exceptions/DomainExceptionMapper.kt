package au.com.ampol.takehome.domain.exceptions

import android.util.Log
import retrofit2.HttpException
import java.io.IOException
import java.net.HttpURLConnection

object DomainExceptionMapper {
    @JvmStatic
    fun from(throwable: Throwable): DomainException {
        Log.e("Recipe App Exception" ,throwable.message ?: "")
        return when (throwable) {
            is HttpException -> when (throwable.response()?.code()) {
                HttpURLConnection.HTTP_UNAUTHORIZED -> UnauthorisedAccessException(throwable.message)
                HttpURLConnection.HTTP_BAD_REQUEST -> BadRequestException(throwable.message)
                HttpURLConnection.HTTP_FORBIDDEN -> AccessForbiddenException(throwable.message)
                HttpURLConnection.HTTP_NOT_FOUND -> NotFoundException(throwable.message)
                else -> DataRetrievalException(throwable.message)
            }
            is IOException -> InternetConnectionException()
            else -> UnexpectedInternalException(throwable.message)
        }
    }



}