package au.com.ampol.takehome.repository

import au.com.ampol.takehome.data.api.RecipesItem
import kotlinx.coroutines.flow.Flow

interface RecipeRepository {
    fun getRecipes() : Flow<RecipesItem>
}