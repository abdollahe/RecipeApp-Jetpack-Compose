package au.com.ampol.takehome.presentation.recipelist

sealed class ViewState {
    object Loading : ViewState()
    data class Error(val errorMessage : String) : ViewState()
    data class Ready(
        val recipeList : List<RecipeItemListedPresentation>
    ) : ViewState()
}

enum class RecipeDifficultyLevel(val levelVal : Int) {
    EASY(0),
    NORMAL(1),
    MEDIUM(2),
    HARD(3) ;

    companion object {
        private val map = values().associateBy(RecipeDifficultyLevel::levelVal)
        fun fromInt(type: Int) = map[type] ?: EASY
    }
}


data class RecipeItemListedPresentation(
    val recipeId : String ,
    val recipeName : String,
    val recipeHeadline : String,
    val thumb: String ,
    val difficultyLevel: RecipeDifficultyLevel,
    val calories: String )
