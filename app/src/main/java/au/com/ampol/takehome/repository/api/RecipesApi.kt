package au.com.ampol.takehome.repository.api

import au.com.ampol.takehome.data.api.Recipes
import retrofit2.http.GET

interface RecipesApi {
    @GET("android-test/recipes.json")
    suspend fun getRecipes() : Recipes
}