package au.com.ampol.takehome.domain.exceptions

class DomainExceptionCodes {
    companion object {
        const val RECIPE_DETAILS_FETCH_ERROR = 1
        const val HTTP_UNAUTHORIZED_ACCESS_ERROR = 2
        const val HTTP_BAD_REQUEST_ERROR = 3
        const val HTTP_ACCESS_FORBIDDEN_ERROR = 4
        const val HTTP__NOT_FOUND_ERROR = 5
        const val DATA_RETRIEVAL_ERROR = 6
        const val INTERNET_CONNECTION_ERROR = 7
        const val UNEXPECTED_INTERNAL_ERROR = 8
    }
}