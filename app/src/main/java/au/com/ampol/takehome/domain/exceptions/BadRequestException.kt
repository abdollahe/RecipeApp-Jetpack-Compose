package au.com.ampol.takehome.domain.exceptions

class BadRequestException(message : String? = null) :
    DomainException(DomainExceptionCodes.HTTP_BAD_REQUEST_ERROR , message)