package au.com.ampol.takehome.defaults

import okhttp3.OkHttpClient
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.Retrofit

abstract class EndpointChangableRepository<API>(
    private val mAppConfig: AppConfig,
    private val mClassType: Class<API>,
    private val mOkHttpClient: OkHttpClient,
    private val mConverterFactoryList: Array<Converter.Factory>
) {

    val mApiImpl: API = setupApiImpl()

    private fun setupApiImpl(): API {
        val baseEndpoint = mAppConfig.baseUrl

        val retrofitBuilder: Retrofit.Builder = Retrofit
            .Builder()
            .baseUrl(baseEndpoint)

        val okHTTPClientBuilder = mOkHttpClient
            .newBuilder()

        retrofitBuilder.client(okHTTPClientBuilder.build())

        for (converter in mConverterFactoryList) {
            retrofitBuilder.addConverterFactory(converter)
        }

        return retrofitBuilder
            .build()
            .create(mClassType)
    }
}