package au.com.ampol.takehome.repository

import au.com.ampol.takehome.data.api.RecipesItem
import au.com.ampol.takehome.defaults.AppConfig
import au.com.ampol.takehome.defaults.EndpointChangableRepository
import au.com.ampol.takehome.repository.api.RecipesApi
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import okhttp3.OkHttpClient
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
open class RecipesRepositoryImpl @Inject constructor(
    mAppConfig: AppConfig,
    okHttpClient: OkHttpClient,
    gson: Gson
): RecipeRepository, EndpointChangableRepository<RecipesApi>(
    mAppConfig,
    RecipesApi::class.java,
    okHttpClient,
    arrayOf(GsonConverterFactory.create(gson))
) {

    // An array list to hold the retrieved response - simulates a simple cache
    private val cacheList = mutableListOf<RecipesItem>()

    override fun getRecipes() : Flow<RecipesItem> {
      return flow {
          if(cacheList.isNotEmpty()) {
              cacheList.forEach {
                  emit(it)
              }
          }
          else {
              cacheList.clear()
              val recipes = mApiImpl.getRecipes().onEach {
                  // Simulate caching the response
                  cacheList.add(it)
              }
              recipes.forEach { item ->
                  emit(item)
              }
          }

      }.flowOn(Dispatchers.IO)
    }
}