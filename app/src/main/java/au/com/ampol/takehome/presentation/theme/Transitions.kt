package au.com.ampol.takehome.presentation.theme

import androidx.compose.animation.AnimatedContentScope
import androidx.compose.animation.EnterTransition
import androidx.compose.animation.ExitTransition
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.core.tween
import androidx.compose.animation.fadeIn
import androidx.compose.animation.slideInHorizontally
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.navigation.NavBackStackEntry


@ExperimentalAnimationApi
val slideUpEntryTransition: AnimatedContentScope<NavBackStackEntry>.() -> EnterTransition? = {
    slideInVertically(
        initialOffsetY = { 1000 },
        animationSpec = tween(300)
    ) + fadeIn(animationSpec = tween(300))
}

@ExperimentalAnimationApi
val slideDownExitTransition: AnimatedContentScope<NavBackStackEntry>.() -> ExitTransition? = {

    slideOutVertically(
        targetOffsetY = { 2000 },
        animationSpec = tween(300)
    )
}

@ExperimentalAnimationApi
val popOutEntryTransition: AnimatedContentScope<NavBackStackEntry>.() -> EnterTransition? =
    {
        slideInHorizontally(
            initialOffsetX = { -300 },
            animationSpec = tween(300)
        ) + fadeIn(animationSpec = tween(300))
    }



