package au.com.ampol.takehome.defaults.di

import android.app.Application
import android.content.Context
import android.content.res.Resources
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    fun provideApplication(context: Context): Application = context.applicationContext as Application

    @Provides
    fun provideResources(context: Context): Resources = context.resources


    /**
     * Dagger provides okhttpclient for all http calls
     * @return
     */
    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient =
        OkHttpClient.Builder()
            .connectTimeout(10000, TimeUnit.MILLISECONDS)
            .readTimeout(10000, TimeUnit.MILLISECONDS)
            .build()

    @Provides
    @Singleton
    fun provideGson(): Gson =
        GsonBuilder()
            .serializeNulls()
            .setDateFormat(JSON_DATE_FORMAT)
            .create()

    companion object {
        const val JSON_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ"
    }
}