package au.com.ampol.takehome.domain.exceptions

class InternetConnectionException(message : String? = null) :
    DomainException(DomainExceptionCodes.INTERNET_CONNECTION_ERROR , message)