package au.com.ampol.takehome.domain.exceptions

class DataRetrievalException(message : String? = null) :
    DomainException(DomainExceptionCodes.DATA_RETRIEVAL_ERROR , message)