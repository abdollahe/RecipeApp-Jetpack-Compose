package au.com.ampol.takehome.domain.usecases


import android.content.res.Resources
import au.com.ampol.takehome.R
import au.com.ampol.takehome.data.api.RecipesItem
import au.com.ampol.takehome.domain.exceptions.DomainExceptionMapper
import au.com.ampol.takehome.domain.exceptions.RecipeDetailsSearchException
import au.com.ampol.takehome.presentation.recipedetails.RecipeItemPresentation
import au.com.ampol.takehome.presentation.recipelist.RecipeDifficultyLevel
import au.com.ampol.takehome.repository.RecipeRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext
import javax.inject.Inject

class FetchRecipeDetailsUseCase @Inject constructor(
    private val recipeRepository: RecipeRepository,
    private val resources: Resources
) {

    suspend fun getRecipeDetails(defaultDispatcher : CoroutineDispatcher = Dispatchers.Default , recipeId : String) : RecipeItemPresentation {
        return try {
           withContext(defaultDispatcher) {
               recipeRepository.getRecipes().filter {
                   it.id == recipeId
               }.map {
                   mapRecipeToPresentation(it)
               }.first()
           }
        }
        catch(e : Exception) {
            throw RecipeDetailsSearchException(resources.getString(R.string.retrieve_recipe_details_error_message))
        }
    }

    private fun mapRecipeToPresentation(recipeItem : RecipesItem) : RecipeItemPresentation {
        return RecipeItemPresentation(
            recipeName = recipeItem.name ,
            recipeHeadline = recipeItem.headline,
            image = recipeItem.image,
            calories = recipeItem.calories,
            difficultyLevel = RecipeDifficultyLevel.fromInt(recipeItem.difficulty) ,
            description = recipeItem.description,
            proteins = recipeItem.proteins
        )
    }

}