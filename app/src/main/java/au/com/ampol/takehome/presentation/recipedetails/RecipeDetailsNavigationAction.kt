package au.com.ampol.takehome.presentation.recipedetails

sealed class RecipeDetailsNavigationAction {
    object GoBackAction : RecipeDetailsNavigationAction()
    object CloseApplication : RecipeDetailsNavigationAction()
}