package au.com.ampol.takehome.presentation.recipelist

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import au.com.ampol.takehome.R
import au.com.ampol.takehome.domain.exceptions.InternetConnectionException
import au.com.ampol.takehome.domain.usecases.FetchRecipeListUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

class RecipeListViewModel @Inject constructor(
    private val recipeListUseCase: FetchRecipeListUseCase,
    val resources : Resources
) : ViewModel() {

    private val _navigationSharedFlow = MutableSharedFlow<RecipeListNavigationAction>()
    val navigationFlow : Flow<RecipeListNavigationAction> = _navigationSharedFlow.asSharedFlow()

    private val _viewStateFlow = MutableStateFlow<ViewState>(ViewState.Loading)
    val viewState = _viewStateFlow.asStateFlow()

    fun prepareViewModel() = getRecipeList()

    private fun getRecipeList() {
        viewModelScope.launch {
            try {
                val recipeList = recipeListUseCase.getRecipeListForPresentation(Dispatchers.IO)
                _viewStateFlow.value = ViewState.Ready(recipeList)
            }
            catch(e : Exception) {
                handleError(e)
            }
        }
    }

    fun goToNextScreen(selectedId : String) {
        viewModelScope.launch {
            _navigationSharedFlow.emit(RecipeListNavigationAction.ShowRecipeDetails(selectedId))
        }
    }

    fun closeApplication() {
        viewModelScope.launch {
            _navigationSharedFlow.emit(RecipeListNavigationAction.CloseApplication)
        }
    }

    fun refreshScreen() {
        _viewStateFlow.value = ViewState.Loading
        viewModelScope.launch {
            // This delay is here to help show the loading progress bar when refresh is pressed.
            delay(500L)
            getRecipeList()
        }
    }

    private fun handleError(e : Exception) {
        when(e) {
            is InternetConnectionException -> {
                _viewStateFlow.value = ViewState.Error(resources.getString(R.string.recipe_list_no_internet_error_message))
            }
            else -> {
                _viewStateFlow.value = ViewState.Error(resources.getString(R.string.recipe_list_general_error_message))
            }
        }
    }


}