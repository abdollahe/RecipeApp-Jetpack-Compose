package au.com.ampol.takehome.defaults.di.viewmodel

import androidx.lifecycle.ViewModel
import au.com.ampol.takehome.presentation.recipedetails.RecipeDetailsViewModel
import au.com.ampol.takehome.presentation.recipelist.RecipeListViewModel
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import javax.inject.Provider

@Module
class ViewModelModule {

    @Provides
    fun provideViewModelFactory(
        viewModels: MutableMap<Class<out ViewModel>, Provider<ViewModel>>
    ): ViewModelFactory {
        return ViewModelFactory(viewModels)
    }

    @Provides
    @IntoMap
    @ViewModelKey(RecipeListViewModel::class)
    fun provideRecipeListViewModel(viewModel: RecipeListViewModel): ViewModel = viewModel

    @Provides
    @IntoMap
    @ViewModelKey(RecipeDetailsViewModel::class)
    fun provideRecipeDetailsViewModel(viewModel: RecipeDetailsViewModel): ViewModel = viewModel


}