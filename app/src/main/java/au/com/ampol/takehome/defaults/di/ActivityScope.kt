package au.com.ampol.takehome.defaults.di

import javax.inject.Scope

/**
 * Defines a dagger scope that only lasts for the life of an activity.
 */
@Scope
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class ActivityScope
