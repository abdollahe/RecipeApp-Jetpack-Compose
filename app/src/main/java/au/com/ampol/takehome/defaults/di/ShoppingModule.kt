package au.com.ampol.takehome.defaults.di

import au.com.ampol.takehome.defaults.AppConfig
import au.com.ampol.takehome.repository.RecipeRepository
import au.com.ampol.takehome.repository.RecipesRepositoryImpl
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import javax.inject.Singleton

@Module
class ShoppingModule {

    @Provides
    @Singleton
    fun provideRecipesRepositoryImpl(
        appConfig: AppConfig,
        okHttpClient: OkHttpClient,
        gson: Gson
    ): RecipesRepositoryImpl {
        return RecipesRepositoryImpl(
            appConfig,
            okHttpClient,
            gson
        )
    }

    @Provides
    @Singleton
    fun providesRecipeRepository(recipeRepositoryImpl : RecipesRepositoryImpl) : RecipeRepository {
        return (recipeRepositoryImpl as RecipeRepository)
    }
}