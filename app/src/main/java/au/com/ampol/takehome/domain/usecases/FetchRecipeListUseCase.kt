package au.com.ampol.takehome.domain.usecases


import au.com.ampol.takehome.data.api.RecipesItem
import au.com.ampol.takehome.domain.exceptions.DomainExceptionMapper
import au.com.ampol.takehome.presentation.recipelist.RecipeDifficultyLevel
import au.com.ampol.takehome.presentation.recipelist.RecipeItemListedPresentation
import au.com.ampol.takehome.repository.RecipeRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.withContext
import javax.inject.Inject

class FetchRecipeListUseCase @Inject constructor (
    private val recipeRepository: RecipeRepository
) {

    suspend fun getRecipeListForPresentation(defaultDispatcher : CoroutineDispatcher = Dispatchers.Default) : List<RecipeItemListedPresentation> {
        return try {
            withContext(defaultDispatcher) {
                recipeRepository.getRecipes().map {
                    mapRecipesToPresentation(it)
                }.toList()
            }
        }
        catch(e : Exception) {
            throw DomainExceptionMapper.from(e)
        }
    }


    private fun mapRecipesToPresentation(recipeItem : RecipesItem) : RecipeItemListedPresentation {
        return RecipeItemListedPresentation(
            recipeId = recipeItem.id,
            recipeName = recipeItem.name ,
            recipeHeadline = recipeItem.headline,
            thumb = recipeItem.thumb,
            calories = recipeItem.calories,
            difficultyLevel = RecipeDifficultyLevel.fromInt(recipeItem.difficulty)
        )
    }

}