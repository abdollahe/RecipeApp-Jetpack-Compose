

# Notes and Assumptions on development.
- The focus for the development was towards the interactions of the classes and how data and control flows amongst the application not on the UI design.
- The UI design for both screen is fairly basic with respect to UX and aesthetics but the compose based UI does demonstrate how recomposition is managed and how state is retrieved from the viewmodel for population of the screens.
- Some minor changes has been made to the Retrofit client to use Kotlin Flows instead of Rxjava for emission of the api responses.
- Git and version control and branching strategies have not been used during development given the time and scale of the test project.

# Ampol Take Home

Expected completion time: ~ 2-3 hours

Purpose of this take home is to assess knowledge in practice.
The take home covers the following principles and will be used to assess the application in a project:

- Dagger Injection
- Domain layer creation and interaction
- Propagation of data from the domain through to a view
- Use of MVVM
- Use of the Observable pattern

This take home will not assess design skills, as such, do not spend a lot of time on what the application looks like.


### Task

Create a simple application that reads from a collection of recipes and displays them within a list on the main page.

The list of recipes can be found here: https://hf-android-app.s3-eu-west-1.amazonaws.com/android-test/recipes.json

Each item view in the list must show the following:
- Recipe Name
- Recipe Headline (as subtitle)

The following are optional items to display within the list
- Recipe Thumbnail
- Recipe Calories
- Recipe difficulty

Additionally to the optional items, time permitting, you may implement a detail view which opens when a user clicks on the cell. This should show the following:
- Recipe Name
- Recipe Headline (as subtitle)
- Recipe description
- Recipe calories
- Recipe proteins
- Recipe Image (Optional)

#### Expectations
You will be expected to use dependency injection to inject both the viewModel into the view, and the repository into the viewModel.

You will also be expected to use Observables for the flow of data in the application.

Follow the TODOs within the application to get started. The rest of the implementation will be up to you.

### Project
A starting project has been provided along with these instructions which has the majority of boilerplate and setup already completed, ready for you to implement the above features. You are also not required to write any unit testing for this task.

This includes:
- Dagger2 Base injection set up
- Network / Domain and repository base set up
- Application set up

### Questions
If you have any questions, or if you require help, feel free to reach out to jxwilki@ampol.com.au


